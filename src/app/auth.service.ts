import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { User } from './user';

@Injectable({
  providedIn: 'root'
})


export class AuthService {


    //נגדיר משתנים
    user:Observable<User | null>; 



  constructor(private afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState; 
  }


  
  login(email:string, password:string){ 
    return this.afAuth.signInWithEmailAndPassword(email,password); 
  }



  logout(){
    this.afAuth.signOut().then(
      res => {
        this.router.navigate(['/bye'])
      }
    );  
  }


  getUser():Observable <User | null> { 
    return this.user;
  }


  SignUp(email:string, password:string){
    return this.afAuth.createUserWithEmailAndPassword(email,password); 
  }





}
