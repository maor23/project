import { TestBed } from '@angular/core/testing';

import { SeaService } from './sea.service';

describe('SeaService', () => {
  let service: SeaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SeaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
