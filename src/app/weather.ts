export interface Weather {
    lat: number,
    lon: number,
    model,
    parameters,
    levels,
    key,
}
