// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  apiKey: "AIzaSyABkRoRvIfaB-otJ3hKvbQnaPLdN79hYOk",
  authDomain: "project-c3fd5.firebaseapp.com",
  projectId: "project-c3fd5",
  storageBucket: "project-c3fd5.appspot.com",
  messagingSenderId: "816049799849",
  appId: "1:816049799849:web:7e814fa604ccd0c1091fb8"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
