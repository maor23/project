import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeaFormComponent } from './sea-form.component';

describe('SeaFormComponent', () => {
  let component: SeaFormComponent;
  let fixture: ComponentFixture<SeaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeaFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeaFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
