import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { SeaService } from '../sea.service';
import { Weather } from '../weather';


@Component({
  selector: 'app-sea-condition',
  templateUrl: './sea-condition.component.html',
  styleUrls: ['./sea-condition.component.css']
})


export class SeaConditionComponent implements OnInit {

  constructor(private route:ActivatedRoute, private seaService:SeaService) { }

    //נגדיר את המשתנים ואת הסוג שלהם
    //city:string; 
    lat:number; 
    lon:number; 
    model; 
    parameters;
    levels;   
    key; 
    weatherData$:Observable<Weather>; //מוסכמה של אובסרבל שאם מוסיפים דולר למשתנה - זה אומר שנחבר למשתנה הזה אובסרבל. המידע שהאובסרבל יפלוט יהיה מסוג ווטר 
    hasError:Boolean = false; // הדיפולטיבי זה שאין שגיאה
    errorMessage:string; 

    cities:Object[] = [{id:1,name:'Jaffa', lon:34.751 ,lat:32.052},{id:2,name:'Hertzelia',lon:34.843 ,lat:32.166}] //העיר האחרונה לא קיימת בכוונה
    city:string;

  
  
    classify(){
      this.seaService.classify(this.lon, this.lat).subscribe(
        res => { //התשובה שמחוזרת מהשרת של הסיווג
          console.log(res); //נדפיס כדי לראות בקונסול את התשובה שמחוזרת מהשרת אם הסיווג
          this.model = res.model; 
          this.parameters = res.parameters;
          this.levels = res.levels;
          this.lat = res.lat;
          this.lon = res.lon;

        }
      )
    }
    

  
  ngOnInit(): void {

        
    
    /*

        //משיכת הפרמטר חובה מה-יו אר אל
        //this.city = this.route.snapshot.params.city; // נתנו את השם סיטי בסוף כי זה השם שהגדרנו בקובץ של אפפ-ראוטינג,נתפוס את המשתנה מהיואראל
        this.weatherData$ = this.weatherService.searchWeatherData();  //הפונקציה סראצ'ווטרדאטה שנמצאת בסרביס, מחזירה אובסרבל  
        this.weatherData$.subscribe( // נרשם לאובסרבל, בשביל שכשיגיעו נתונים נוכל לתפוס אותם
          data => { // כאשר אין שגיאה הפונקצייה סאסקרייב תפעיל את זה ותראה את הנתונים
            this.model = data.model; //בגלל השם שנתנו לפונקציה בשורה למעלה בשם "דאטה", גם כאן צריך לכתוב דאטה
            this.parameters = data.parameters;
            this.levels = data.levels;
            this.lat = data.lat;
            this.lon = data.lon;
          },
          error =>{ //כאשר יש שגיאה הפונקציה סאסקרייב תפעיל את זה
            console.log(error.message)//בגלל השם שנתנו לפונקציה בשורה למעלה בשם "ארור", גם כאן צריך לכתוב ארור, נדפיס את השגיאה לקונסול
            this.hasError = true; //נשנה מהערך הדיפולטיבי פאלס, לטרו, משמע יש שגיאה
            this.errorMessage = error.message;
          }
        ) */
      }


  }


