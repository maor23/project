import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeaConditionComponent } from './sea-condition.component';

describe('SeaConditionComponent', () => {
  let component: SeaConditionComponent;
  let fixture: ComponentFixture<SeaConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeaConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeaConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
