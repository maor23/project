import { WelcomeComponent } from './welcome/welcome.component';
import { RegisterComponent } from './register/register.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ByeComponent } from './bye/bye.component';
import { PredictComponent } from './predict/predict.component';
import { SeaConditionComponent } from './sea-condition/sea-condition.component';
import { SeaFormComponent } from './sea-form/sea-form.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent }, 
  { path: 'signup', component: RegisterComponent }, 
  { path: 'welcome', component: WelcomeComponent }, 
  { path: 'bye', component: ByeComponent }, 
  { path: 'predict', component: PredictComponent }, 
  { path: 'seaCondition', component: SeaConditionComponent }, 
  { path: 'city', component: SeaFormComponent }, //הראצה 6
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
